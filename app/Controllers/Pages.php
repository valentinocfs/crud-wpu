<?php

namespace App\Controllers;

class Pages extends BaseController
{
    public function index()
    {
        $data = [
            'title' => 'Home | KICIW'
        ];
        return view('pages/home', $data);
    }

    public function About()
    {
        $data = [
            'title' => 'About Me | KICIW'
        ];
        return view('pages/about', $data);
    }

    public function Contact()
    {
        $data = [
            'title' => 'Contact | KICIW'
        ];
        return view('pages/contact', $data);
    }
}
