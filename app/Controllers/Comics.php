<?php

namespace App\Controllers;

use App\Models\ComicModel;
use Config\Services;

class Comics extends BaseController
{
    protected $comicModel;
    public function __construct()
    {
        $this->comicModel = new ComicModel();
    }

    public function index()
    {
        $data = [
            "title" => "Comics | KICIW",
            "comics" => $this->comicModel->getComic()
        ];

        return view('/comics/index', $data);
    }

    public function detail($slug)
    {
        $data = [
            "title" => "DETAIL | KICIW",
            "comic" => $this->comicModel->getComic($slug)
        ];

        if (empty($data['comic'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Comic ' . $slug . ' not found.');
        }

        return view('comics/detail', $data);
    }

    public function create()
    {
        // session();
        $data = [
            "title" => 'Add Comic',
            "validation" => \Config\Services::validation()
        ];

        return view('comics/create', $data);
    }

    public function save()
    {
        if (!$this->validate([
            'title' => [
                'rules' => 'required',
                'errors' => [
                    'required' => "The Title must have a contains."
                ]
            ],
            'writter' => 'required',
            'publisher' => 'required'
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/comics/create')->withInput()->with('validation', $validation);
        }

        $slug = url_title($this->request->getVar('title'), '-', true);
        $this->comicModel->save([
            'title' => $this->request->getVar('title'),
            'slug' => $slug,
            'writter' => $this->request->getVar('writter'),
            'publisher' => $this->request->getVar('publisher')
        ]);

        session()->setFlashdata('message', 'Success creating data.');

        return redirect()->to('/comics');
    }

    public function delete($id)
    {
        $this->comicModel->delete($id);

        session()->setFlashdata('message', 'Success delete the data.');

        return redirect()->to('/comics');
    }
}
