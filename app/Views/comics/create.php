<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>
<div class="container my-5">
    <div class="row">
        <div class="col-8">
            <h3 class="mb-4">Form</h3>
            <form action="/comics/save" method="post">
                <?= csrf_field(); ?>
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control <?= ($validation->hasError('title')) ? 'is-invalid' : '' ?>" id="title" name="title" placeholder="Input title comic" value="<?= old('title'); ?>" autofocus>
                    <div class="invalid-feedback">
                        <?= $validation->getError('title'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="writter">Writter</label>
                    <input type="text" class="form-control <?= ($validation->hasError('writter')) ? 'is-invalid' : '' ?>" id="writter" name="writter" placeholder="Input writter comic" value="<?= old('writter'); ?>">
                    <div class="invalid-feedback">
                        <?= $validation->getError('writter'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="publisher">Publisher</label>
                    <input type="text" class="form-control <?= ($validation->hasError('publisher')) ? 'is-invalid' : '' ?>" id="publisher" name="publisher" placeholder="Input publisher comic" value="<?= old('publisher'); ?>">
                    <div class="invalid-feedback">
                        <?= $validation->getError('publisher'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="cover">Cover</label>
                    <input type="text" class="form-control" id="cover" name="cover" placeholder="Input cover comic" value="<?= old('cover'); ?>">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>