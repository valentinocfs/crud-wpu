<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>
<div class="container my-5">
    <div class="row">
        <div class="col-md-8">
            <h1>Comics</h1>
            <p>You can add many more comic in <a href="/comics/create">here</a></p>
            <div class="card mb-3 p-3" style="max-width: 540px;">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="/img/<?= $comic['cover']; ?>" class="card-img" alt="<?= $comic['title']; ?>">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">
                                <?= $comic['title']; ?>
                            </h5>
                            <p class="card-text"><strong>Writter : </strong><?= $comic['writter']; ?></p>
                            <p class="card-text"><strong>Publisher : </strong><?= $comic['publisher']; ?></p>
                            <div class="mt-5">
                                <a href="#" class="btn btn-success">Edit</a>
                                <form action="/comics/<?= $comic['id']; ?>" method="post" class="d-inline">
                                    <?= csrf_field(); ?>
                                    <input type="hidden" name="_method" id="" value="DELETE">
                                    <button type="submit" class="btn btn-danger" onclick="return confirm('Delete this item?')">Delete</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 text-right">
            <a href="/comics" class="btn btn-primary">Back</a>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>