<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>
<div class="container my-5">
    <div class="row">
        <div class="col-9">
            <h1>Comics</h1>
            <p class="">Comics Lorem ipsum dolor sit amet.</p>
        </div>
        <div class="col-3">
            <?php if (session()->getFlashdata('message')) : ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->getFlashdata('message'); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a href="/comics/create" class="btn btn-primary mb-5">Add Comic</a>

            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Cover</th>
                        <th scope="col">Title</th>
                        <th scope="col">Detail</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($comics as $key) : ?>
                        <tr>
                            <th scope="row"><?= $i++; ?></th>
                            <td>
                                <img src="/img/<?= $key['cover'] ?>" class="cover-comic" alt="<?= $key['title']; ?>" />
                            </td>
                            <td><?= $key['title'] ?></td>
                            <td>
                                <a href="/comics/<?= $key['slug'] ?>" class="btn btn-primary">Detail</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>